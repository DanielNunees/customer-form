## My Thoughts to Create The Form. 

I tried to create in my mind a fake situation, where the real customer form, for some reason, stopped worked, 
so i was invited to fix that ASAP, because we are actually running a campaign and losing potential leads.

## Environment Dependencies
 - Jquery
 - BootStrap

## Database(MySql 5.6) 
 - Database Name: clickview
 - Credentials:
   -- username: root
   -- password: 
 - Tables
    --customers



## Server
 - Apache 2.4 (XAMPP) 

## PHP
 - Version 7.1.23

 
## Create database
 - Open the folder 'SQL-CREATE DATABASE'
 - run the sql

## Running the project

- Download the repository
- `cp /project-folder` to your server folder
- access localhost/project-folder/index.html
- enjoy


## `GET /list`
- Return a list with all customers


## `POST: /register`
- Register a new customer
