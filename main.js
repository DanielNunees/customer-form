$(document).ready(function () {
    /**
     * Save customer on the database
     */
    $("#alert").hide();
    $("#contact-form").on("submit", function (e) {
        $("#loading").modal('show');
        e.preventDefault();
        var sendData = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "./services/register.php",
            data: getFormData(),
            success: function (data) {
                if (data === "true") {
                    $("#alert").show();
                }
                endLoading();
            }
        });
    });
    getCustomers();
});



/**
 * Get form input values from user
 */
const getFormData = () => {
    let data = {
        //Personal Details
        name: $("#name").val(),
        last_name: $("#last_name").val(),
        email: $("#email").val(),
        phone: $("#phone").val(),
        //Address
        city: $("#city").val(),
        country: $("#country").val(),
        //Services
        role: $("#role").val(),
        school_name: $("#school_name").val(),
        service: $("#service").val(),
        find_us: $("#find_us").val(),
        message: $("#message").val(),
    }
    return data;
}


/**
 * Get All customers from database
 */
const getCustomers = () => {
    $.ajax({
        type: "GET",
        url: "./services/list.php",
        dataType: "JSON",
        success: function (data) {
            setCustomerList(data);
        }
    });
}

/**
 *  Set list of all users
 * @param {Object} data 
 */
const setCustomerList = (data) => {
    $("#loading").modal('show');
    $('#customer_table tr').not(':first').not(':last').remove();

    data.forEach(customer => {
        var html = '';
        html += '<tr>' +
            '<td>' + customer['id'] + '</td>' +
            '<td>' + customer['name'] + " " + customer['last_name'] + '</td>' +
            '<td>' + customer['email'] + '</td>' +
            '<td>' + customer['phone'] + '</td>' +
            '<td>' + customer['role'] + '</td>' +
            '<td>' + customer['school_name'] + '</td>' +
            '<td>' + customer['city'] + '</td>' +
            '<td>' + customer['country'] + '</td>' +
            '<td>' + customer['service'] + '</td>' +
            '<td>' + customer['find_us'] + '</td>' +
            '<td>' + customer['message'] + '</td>' +
            + '</tr>';

        $('#customer_table tr').first().after(html);

    });
    endLoading();

}


/**
 * Hide loading
 */
const endLoading = () => {
    setTimeout(function () {
        $("#loading").modal("hide");
    }, 500);

}


