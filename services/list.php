<?php
require("./connection.php");



$sql = "SELECT * FROM customer";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    // output data of each row
    $i=0;
    while ($row = $result->fetch_assoc()) {
        
        $data[$i]["id"] = $row["id"];
        $data[$i]["name"] = $row["name"];
        $data[$i]["last_name"] = $row["last_name"];
        $data[$i]["email"] = $row["email"];
        $data[$i]["phone"] = $row["phone"];

        $data[$i]["role"] = $row["role"];
        $data[$i]["school_name"] = $row["school_name"];
        $data[$i]["city"] = $row["city"];
        $data[$i]["country"] = $row["country"];
        $data[$i]["service"] = $row["service"];

        $data[$i]["find_us"] = $row["find_us"];
        $data[$i]["message"] = $row["message"];
        $i++;
    }
    echo json_encode($data);
} else {
    echo "0 results";
}
$conn->close();
